import pytest
import pytest_mock
import requests
import json
import os
import events
import re
from datetime import datetime

@pytest.fixture
def expected_json(request):
    filename = request.module.__file__
    test_dir, _ = os.path.split(filename)
    json_path = os.path.join(test_dir, "expected_data.json")
    with open(json_path, 'r') as fp:
        json_data = json.load(fp)
    return json_data

def test_testing():
    pass

def test_API_online():
    r = requests.get("http://open-api.myhelsinki.fi/v1/events/?limit=1")
    assert r.ok
    assert r.status_code == 200

def test_get_data_ok(expected_json, mocker):
    mock_get = mocker.patch('requests.get')
    mock_get.json.return_value = expected_json
    mock_get.return_value.ok = True
    mock_get.return_value.status_code = 200

    data = events.get_data.uncached() # Mock breaks flask cache

    mock_get.assert_called_once()
    assert all([a == b for a, b in zip(data, expected_json["data"])])

def test_get_data_bad(mocker):
    mock_get = mocker.patch('requests.get')
    mock_get.return_value.ok = False

    with pytest.raises(Exception):
        events.get_data.uncached() # Mock breaks flask cache

    mock_get.assert_called_once()

def test_weekstring_format():
    current_weekstring = events.dt_local_weekstring(datetime.now())
    assert re.match(r"[1-2][0-9]{3}-W[0-5][0-9]", current_weekstring) != None

def test_sanitize_weekstring():
    current_weekstring = events.dt_local_weekstring(datetime.now())
    
    assert events.sanitize_weekstring(None) == current_weekstring
    assert events.sanitize_weekstring("Bad format") == current_weekstring
    assert events.sanitize_weekstring("2020-W04") == "2020-W04"

def test_get_events_by_week_fields(expected_json, mocker):
    mock_events = mocker.patch('events.get_data')
    mock_events.return_value = expected_json["data"]

    result = events.get_events_by_week("2020-W04")

    assert all(["name" in r.keys() for r in result])
    assert all(["time" in r.keys() for r in result])
    assert all(["location" in r.keys() for r in result])
    
def test_get_events_by_week_times(expected_json, mocker):
    mock_events = mocker.patch('events.get_data')
    mock_events.return_value = expected_json["data"]
    
    result = events.get_events_by_week("2020-W04")

    assert all([events.dt_local_weekstring("2020-W04")  for r in result])

def test_get_page():
    with events.app.test_client() as c:
        rv = c.get("/")
        assert b'html' in rv.data

def test_get_events_ok(mocker, expected_json):
    mock_events = mocker.patch('events.get_data')
    mock_events.return_value = expected_json["data"]

    with events.app.test_client() as c:
        rv = c.get("/events/2020-W05")
        assert b'location' in rv.data
        assert b'time' in rv.data
        assert b'name' in rv.data

def test_get_events_bad(mocker):
    mock_events = mocker.patch('events.get_data')
    mock_events.side_effect = Exception()

    with events.app.test_client() as c:
        rv = c.get("/events/2020-W05")
        assert rv.status_code == 500
