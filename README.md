# helsinki-events

A simple web application for displaying events in Helsinki for the current week.

- Uses [flask](https://www.palletsprojects.com/p/flask/) for a simple python backend.

- Frontend logic and templating with [vue.js](https://vuejs.org/)

- Accesses [Helsinki Open-API](http://open-api.myhelsinki.fi/) for event data.

Tested with Ubuntu 18.04 and Google Chrome version 79.0.3945

---

### Installation 

See [install](./install) to set up the build environment.

---

### Deploying

See [deploy-dev](./deploy-dev) to launch a the backend locally in development mode.

---

### Testing

Uses packages:
- [pytest](https://docs.pytest.org/en/latest/)
- [pytest-mock](https://pypi.org/project/pytest-mock/) 
- [coverage](https://coverage.readthedocs.io/en/coverage-5.0.3/)

See [run-tests](./run-tests) to run unit tests and check coverage.

---

### Build status

Unit tests are run automatically in the gitlab CI pipeline. Current status:

![](https://gitlab.com/psaikko/helsinki-events/badges/master/coverage.svg)
![](https://gitlab.com/psaikko/helsinki-events/badges/master/pipeline.svg)