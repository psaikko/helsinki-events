from flask import Flask, json, abort
from flask_caching import Cache
import requests
import dateutil.parser 
import pytz
import re
from datetime import datetime

app = Flask(__name__)
cache = Cache(config={'CACHE_TYPE': 'simple'})
cache.init_app(app)

@app.route('/')
def hello():
    return app.send_static_file("app.html")

def dt_local_weekstring(dt):
    return dt.astimezone(pytz.timezone("Europe/Helsinki")).strftime("%G-W%V")

def sanitize_weekstring(weekstring):
    # sanitize weekstring format, default to current week
    if weekstring == None or not re.match(r"\d{4}-W\d{2}", weekstring):
        app.logger.debug("Bad weekstring %s" % weekstring)
        weekstring = dt_local_weekstring(datetime.now())
    return weekstring

@cache.cached(timeout=3600, key_prefix='all_events')
def get_data():
    r = requests.get("http://open-api.myhelsinki.fi/v1/events/?language_filter=en")
    if r.status_code != 200:
        raise Exception("Status code %d from API" % r.status_code)
    return r.json()["data"]

def get_events_by_week(query_weekstring):
    events_data = get_data()
    # Filter out events without necessary information
    events_data = [e for e in events_data if e["event_dates"]["starting_day"] != None]

    # Extract relevant fields
    events = [
        {
            "name" : data["name"]["en"] or data["name"]["fi"],
            "location" : data["location"]["address"]["street_address"],
            "time" : dateutil.parser.parse(data["event_dates"]["starting_day"]),
            "url" : data["info_url"]
        }
        for data in events_data
    ]

    # Sort by time 
    events.sort(key=lambda e : e["time"])

    # Filter events on the correct week
    events = [e for e in events if dt_local_weekstring(e["time"]) == query_weekstring]

    return events

@app.route('/events/', defaults={'weekstring':None})
@app.route('/events/<string:weekstring>')
def events(weekstring):
    try:
        res = get_events_by_week(sanitize_weekstring(weekstring))
        return json.dumps(res)
    except Exception as e:
        app.logger.debug(str(e))
        abort(500)
    
